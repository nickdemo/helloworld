package main

import (
	"fmt"
	"helloworld/hello"
	"net/http"
)

func main() {
	fmt.Println("into main")
	http.Handle("/", new(hello.HelloHandler))
	http.Handle("/hello", new(hello.HelloHandler))
	http.Handle("/print/startup", new(hello.PrintStartParam))
	http.Handle("/print/env", new(hello.EnvParam))
	http.Handle("/ping", new(hello.Pong))
	http.Handle("/ping/v1.0.1", new(hello.PongV2))
	http.Handle("/ping/v1.0.2", new(hello.PongV2ex))
	http.Handle("/health", new(hello.HealthHandler))
	http.Handle("/healthz", new(hello.HealthzHandler))
	http.Handle("/print/config/appinfo", new(hello.PrintAppInfo))
	http.Handle("/print/config/authorinfo", new(hello.PrintAuthorInfo))
	http.Handle("/op/redis", new(hello.RedisHandler))
	http.Handle("/resource/cpu", new(hello.Cpu))
	go http.ListenAndServeTLS(":443", "cert/tls.crt", "cert/tls.key", nil)
	http.ListenAndServe(":80", nil)
	//	http.ListenAndServeTLS()
}
