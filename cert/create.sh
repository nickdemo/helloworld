#!/bin/bash
cn=localhost
o=my-hello
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /tmp/tls.key -out /tmp/tls.crt -subj "/CN=$cn/O=$o"
