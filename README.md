# helloworld
##说明
* 用于golang云原生部分，操作演示代码
## 接口
`/`

`/hello`
* 打印hello world

`/print/startup`
* 打印启动参数

`/print/env`
* 打印环境变量

`/ping`
* 打印ip地址
